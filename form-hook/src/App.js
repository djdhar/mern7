import { useState } from "react";
import { useForm } from "react-hook-form";

function App() {
  const { register, handleSubmit, formState: { errors }} = useForm();
  const [userInfo, setUserInfo] = useState()
  const onSubmit = (data) => {
     setUserInfo(data)
     console.log(data);
  };

  // console.log(errors)

  return (
    <div className="App" >
      <pre>{JSON.stringify(userInfo)}</pre>
      <h4>Registration Form</h4>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>Username</label><br/>
        <input name="username" type="test" placeholder="Username" {...register("username", {required: "UserName is required!"})}/><br/>
        <p>{errors.username?.message}</p>
        <label>Email Id</label><br/>
        <input name="email" type="text" 
              placeholder="Email Id" 
              {...register("email", 
                {
                  required: "Email Id is required!", 
                  pattern: {
                    value: /^\S+@\S+$/i,
                    message: "This is not a valid email",
                  }})
              }/><br/>
        <p>{errors.email?.message}</p>
        <label>Password</label><br/>
        <input name="password" type="password" 
            placeholder="Password" 
            {...register("password", 
              {
                required: "Password is required!",
                minLength: {
                  value: 4,
                  message: "Password must be more than 4 characters",
                },
                maxLength: {
                  value: 10,
                  message: "Password cannot exceed more than 10 characters",
                },
              })
            }/><br/>
        <p>{errors.password?.message}</p>
        <button>Submit</button>
      </form>
    </div>
  );
}

export default App;
